module Main where

doubleMe x = x + x

doubleUs x y = doubleMe x + doubleMe y


doubleSmall x = if x > 100
then x
else doubleMe x


-- Function implements factorial calculation by using recursion
-- On invalid input returns (-1)
fac :: (Integral a) => a -> a
fac 0 = 1

fac n = if n < 0
then (-1)
else fac (n - 1) * n


-- Factorial function using guards
fac' :: (Integral a) => a -> a
fac' x
    | x == 0 = 1
    | x < 0 = (-1)
    | otherwise = x * fac' (x - 1)




dot :: (Num a) => (a, a, a) -> (a, a, a) -> a
dot (x1, y1, z1) (x2, y2, z2) = x1 * x2 + y1 * y2 + z1 * z2



maxInList :: (Ord a) => [a] -> a
maxInList [] = error "Empty list"
maxInList [x] = x
maxInList (h:tail)
    | h > tailMax = h
    | otherwise = tailMax
    where tailMax = maxInList tail



repl :: (Ord a, Num a) => b -> a -> [b]
repl b a
    | a <= 0 = []
    | a == 1 = [b]
    | otherwise = b:tailList
    where
        numberOfTimes = a - 1
        tailList = repl b numberOfTimes


slice :: [b] -> Int -> [b]
slice b a
    | a > length b = b
    | a <= 0 = []
    | otherwise = headB:tailSection
    where
        headB = head b
        tailB = tail b
        tailSection = slice tailB (a - 1)


rev :: [a] -> [a]
rev a
    | lenA == 0 = []
    | otherwise = revTail ++ [headA]
    where
        lenA = length a
        tailA = tail a
        headA = head a
        revTail = rev tailA


isInside :: (Ord a) => a -> [a] -> Bool
isInside ele list
    | length list == 0 = False
    | head list == ele = True
    | otherwise = isInside ele (tail list)



