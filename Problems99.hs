module Problems99 where

-- Problem #1
-- Find the last element of a list.
-- (Note that the Lisp transcription of this problem is incorrect.)
myLast :: [a] -> a
myLast list
    | lenList == 0 = error "No last element in empty list"
    | lenList == 1 = head list
    | otherwise = myLast (tail list)
    where lenList = length list

-- Problem #2
-- Find the last but one element of a list.
myButLast :: [a] -> a
myButLast [] = error "Empty list"
myButLast [x] = error "No second last element in singleton"
myButLast (x:tail) = if length tail == 1
                        then x
                        else myButLast tail


-- Problem #3
-- Find the K'th element of a list. The first element in the list is number 1.
elementAt :: [a] -> Int -> a
elementAt list i
    | lenList == 0 = error "Empty list"
    | i <= 0 || i == 1 = head list
    | i >= lenList = myLast list
    | otherwise = elementAt (tail list) (i - 1)
    where lenList = length list



-- Problem #4
-- Find the number of elements of a list.
myLength :: [a] -> Int
myLength [] = 0
myLength [x] = 1
myLength (x:tail) = 1 + myLength tail


-- Problem #5
-- Reverse a list.
myReverse :: [a] -> [a]
myReverse [] = []
myReverse [x] = [x]
myReverse (x:tail) = (myReverse tail) ++ [x]



-- Problem #6
-- Find out whether a list is a palindrome. A palindrome can be read forward or backward; e.g. (x a m a x).
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome [] = True
isPalindrome [x] = True
isPalindrome list = let listLen = myLength list
                        isEven = even listLen
                        firstHalf = if isEven
                                    then take (listLen `div` 2) list
                                    else take ((listLen - 1) `div` 2) list

                        secondHalf = if isEven
                                     then drop (listLen `div` 2) list
                                     else drop ((listLen + 1) `div` 2) list

                    in if firstHalf == myReverse secondHalf
                    then True
                    else False


-- Problem #7
-- Flatten a nested list structure.
-- Transform a list, possibly holding lists as elements into a `flat' list by replacing each list with its elements (recursively).




-- Problem #8
-- Eliminate consecutive duplicates of list elements.
-- If a list contains repeated elements they should be replaced with a single copy of the element. The order of the elements should not be changed.
--
-- This implementation ignores the consecutive part ie with input "aabacccc" the output will be "abc"
compress' :: (Eq a) => [a] -> [a]
compress' [] = []
compress' [x] = [x]
compress' (h:t) = h : compress' (removeEntriesFromList t h)


-- This is the implementation that was needed in the problem #8
compress :: (Eq a) => [a] -> [a]
compress [] = []
compress [x] = [x]
compress (h:t) = h : compress (removeHeadEntriesFromList t h)


removeHeadEntriesFromList :: (Eq a) => [a] -> a -> [a]
removeHeadEntriesFromList [] _ = []
removeHeadEntriesFromList (h:t) el = if el == h
                                     then removeHeadEntriesFromList t el
                                     else h:t


removeEntriesFromList :: (Eq a) => [a] -> a -> [a]
removeEntriesFromList list el
    | lenList == 0 = []
    | headList == el = removeEntriesFromList tailList el
    | otherwise = headList:removeEntriesFromList tailList el
    where lenList = myLength list
          headList = head list
          tailList = tail list



-- Problem #9
-- Pack consecutive duplicates of list elements into sublists. If a list contains repeated elements they should be placed in separate sublists.
-- pack ['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e'] == ["aaaa","b","cc","aa","d","eeee"]
pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack [x] = [[x]]
pack (h:t) = let tailNoDupes = removeHeadEntriesFromList t h
                 listHead = fstSeqAsList (h:t) tailNoDupes
             in [listHead] ++ pack tailNoDupes



fstSeqAsList :: (Eq a) => [a] -> [a] -> [a]
fstSeqAsList [] _ = []
fstSeqAsList [x] _ = [x]
fstSeqAsList list lTailNoDupes = let lHead = head list
                                     lTail = tail list
                                     tailLen = myLength lTail
                                     tailLenNoDupes = myLength lTailNoDupes
                                 in take (1 + tailLen - tailLenNoDupes) (repeat lHead)




-- Problem #10
-- Run-length encoding of a list. Use the result of problem P09 to implement the so-called run-length encoding data compression method.
-- Consecutive duplicates of elements are encoded as lists (N E) where N is the number of duplicates of the element E.
-- encode "aaaabccaadeeee" == [(4,'a'),(1,'b'),(2,'c'),(2,'a'),(1,'d'),(4,'e')]
encode :: (Eq a) => [a] -> [(Int, a)]
encode [] = []
encode [x] = [(1, x)]
encode (h:t) = let tailNoHeadDupes = removeHeadEntriesFromList t h
                   diffInLen = (myLength t) - (myLength tailNoHeadDupes)
               in [(1 + diffInLen, h)] ++ encode tailNoHeadDupes